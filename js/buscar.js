'use strict';

const RIDESKEY = 'nuevo';


function seeRide(element) {
    const dataObj = jQuery(element).data();
    let auth = JSON.parse(localStorage.getItem('nuevo'));
    let found;
    auth.forEach(function (auth) {
        if (auth.id == dataObj.id) {
            found = auth;
            return;
        }
    });
    location.href = "verRide.html?id=" + found.id + "";
    $('#id').val(found.id);
    $('#nombre').val(found.nombre);
    $('#desde').val(found.desde);
    $('#hasta').val(found.hasta);
    $('#mensaje').val(found.mensaje);
    $('#sal').val(found.salida);
    $('#lle').val(found.llegada);
    $('#lun').prop('checked', found.lun);
    $('#mar').prop('checked', found.mar);
    $('#mie').prop('checked', found.mie);
    $('#jue').prop('checked', found.jue);
    $('#vie').prop('checked', found.vie);
    $('#sab').prop('checked', found.sab);
    $('#dom').prop('checked', found.dom);
}




//-------------------------------------------------------------
function renderTable(tableName, tableData) {
    const from = document.getElementById('des_b').value;
    const to = document.getElementById('has_b').value;

    if (from != "" || to != "") {
        let rides = JSON.parse(localStorage.getItem(RIDESKEY));
        let table = $('#' + tableName + '_table');
        let rows = "";
        tableData.forEach((rides, index) => {
            if (rides.desde == from && rides.hasta == to) {
                let row = `<tr>`;
                row += `<td>${rides.id}</td>`;
                row += `<td>${rides.nombre}</td>`;
                row += `<td>${rides.desde}</td>`;
                row += `<td>${rides.hasta}</td>`;
                row += `<td> <a onclick="seeRide(this)" data-id="${rides.id}" data-entity="${tableName}" class="link edit">Ver</a></td>`;
                rows += row + '</tr>';
                table.html(rows);
            } 
        });
    } else {
        alert("Ingrese al menos un parámetro para filtrar");
    }
}


/**
* obtiene datos
*
* @param {*} tableName el nombre de la tabla para mostrar
*/
function getTableData(tableName) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
        tableData = [];
    }
    return tableData;
}

function loadTableDataa(tableName) {
    renderTable(tableName, getTableData(tableName));
}

function clearFieldsR() {
    $('#ride-name').val("");
    $('#start').val("");
    $('#end').val("");
    $('#descp').val("");
    $('#ride-departure').val("");
    $('#ride-arrival').val("");
    $('#lunes').prop('checked', false);
    $('#martes').prop('checked', false);
    $('#miercoles').prop('checked', false);
    $('#jueves').prop('checked', false);
    $('#viernes').prop('checked', false);
    $('#sabado').prop('checked', false);
    $('#domingo').prop('checked', false);
}

function loadEditInfoRide() {
    clearFieldsR();
    var rideId = getParameterByName('id');
    if (rideId != "") {

        let auth = JSON.parse(localStorage.getItem('nuevo'));
        let found;
        auth.forEach(function (auth) {
            if (auth.id == rideId) {
                found = auth;
                return;
            }
        });
        $('#id').val(found.id);
    $('#nombre').val(found.nombre);
    $('#desde').val(found.desde);
    $('#hasta').val(found.hasta);
    $('#mensaje').val(found.mensaje);
    $('#sal').val(found.salida);
    $('#lle').val(found.llegada);
    $('#lun').prop('checked', found.lun);
    $('#mar').prop('checked', found.mar);
    $('#mie').prop('checked', found.mie);
    $('#jue').prop('checked', found.jue);
    $('#vie').prop('checked', found.vie);
    $('#sab').prop('checked', found.sab);
    $('#dom').prop('checked', found.dom);
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

