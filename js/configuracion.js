function guardarDato(){
    let nombre = document.getElementById('name').value;
    let velocidad = document.getElementById('vel').value; 
    let sobreM = document.getElementById('mess').value;
   
const dato = {
   nombre,
   velocidad,
   sobreM,
  
};

return dato;
}



 /**
  * Obtiene a un usuaio de una tabla
  */
 function obtenerUsuario(tableName) {
   let nombreUsuario="";
   let session= obtenerDatosSession(tableName);
   for(let i in session){
     nombreUsuario= session[i].nombreUsuario;
     break;
   }
   return nombreUsuario;
 }

 /**
  * Obtiene el id de una tabla de usuarios
  * @param nombreTabla nombre de la tabla
  */
 function obtenerIdUsuario(nombreTabla,nombreUsuario){
   let datos=obtenerDatosTabla(nombreTabla);
   let id="";
   for(let i in datos){
     if(nombreUsuario==datos[i].nombreUsuario){
       id=datos[i].id;
       break;
     }
   }
   return id;
 }

/**
 * metodos de localStorage y sessionStorage
 */
 /**
 


 /**
  * Inserta los datos del usuaroio en la tabla usuario del localStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
*/
  function insertarEnTabla(tableName, object) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
      tableData = [];
    }
    let primaryKey = tableData.length + 1;
    object.id = primaryKey;
    tableData.push(object);
    localStorage.setItem(tableName, JSON.stringify(tableData));
    return tableData;
  }  

  /**
   * Obtener datos de usuario
   *
   * @param {*} tableName nombre de la tabla
   */
  function obtenerDatosTabla(tableName) {
    let tableData = JSON.parse(localStorage.getItem('datos'));
    console.log(tableData);
    if (!tableData) {
      tableData = [];
    }
    return tableData;
  }

  /**
   *
   * @param {*} llave llave para guardar
   * @param {*} valor valor asociado a la llave
   */
  function guardarLocalStorage(llave, valor){
    localStorage.setItem(key, JSON.stringify(valor));
    return true;
  }

 /**
  * Inserta cualquier objeto en la tabla del sessionStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
  */
function guardarSession(tableName, object){
  let tableData = JSON.parse(sessionStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  let primaryKey = tableData.length + 1;
  object.id = primaryKey;
  tableData.push(object);
  sessionStorage.setItem(tableName, JSON.stringify(tableData));
  return tableData;
}

/**
 * obtiene los datos de una tabla en el sessionStorage
 * @param tableName nombre de la tabla
 */
function obtenerDatosSession(tableName) {
  let tableData = JSON.parse(sessionStorage.getItem(tableName));
  
  if (!tableData) {
    tableData = [];
  }
  return tableData;
}


 function verificarRegistro(){
   let nombreUsuario = document.getElementById('name').value;
   let datos=obtenerDatosTabla('datos');
   let user="";
   for(let i in datos){
     if(nombreUsuario==datos[i].nombreUsuario){
       user=datos[i].nombreUsuario;
       break;
     }
   }
   let dato= guardarDato();
   if(!dato.nombre || !dato.velocidad || !dato.sobreM){
     window.alert("Por favor llenar todos los campos");
   }else{
       if(user!=dato){
         dato=insertarEnTabla('datos', dato);
       }else {
         window.alert("el usuario ya existe");
       }
    
   }
}


function verificarCamposInicio(campoUno, campoDos, nombreTabla, datosTabla){
  if(!campoUno || !campoDos){
    window.alert("No olvides llenar los campos");
  }else{
    renderizarTablaBusqueda(campoUno, campoDos, nombreTabla, datosTabla);
  }
}



 function eventos() {
 	jQuery('#btnReg').bind('click', (element) => {
 		verificarRegistro();
 	});
}

 eventos();
