
/**
 * optiene datos
 *
 * @param {*} tableName el nombre de la tabla para mostrar
 */
function getTableDatas(tableName) {
    let tableData = JSON.parse(localStorage.getItem(tableName));
  
    if (!tableData) {
      tableData = [];
    }
    return tableData;
  }
  

/**
 * Genera una tabla dinamica html 
 *
 * @param tableName
 * @param tableData
 */
function generarTablaGlobal(tableName, tableData) {
	let table = jQuery(`#${tableName}`);

	let rows = "";
	tableData.forEach((nuevo, index) => {
		let row = `<tr><td>${nuevo.nombre}</td>
        <td>${nuevo.desde}</td>
        <td>${nuevo.hasta}</td>
        <td></td>
        <td></td>`;
        row += `<td><a  onclick="deleteEntity(this);" data-id="${nuevo.id}" data-entity="${tableName}">Delete</a>  </td>`;
		rows += row + '</tr>';
	});
	table.html(rows);
}

function deleteEntity(element) {

    if (confirm('Seguro que quiere eliminar?')) {
        const dataObj = jQuery(element).data();
        // const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
        let rides = JSON.parse(localStorage.getItem('nuevo'));
        let results = rides.filter(book => book.id != dataObj.id);
        localStorage.setItem('nuevo', JSON.stringify(results));
        generarTablaGlobal(dataObj.entity, results);
    }
    window.alert("Ride eliminado");
}


function loadTableDataa(tableName) {
	generarTablaGlobal(tableName, getTableData(tableName));
}