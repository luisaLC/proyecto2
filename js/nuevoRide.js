function guardarDato(){
    let nombre = document.getElementById('name').value;
    let desde = document.getElementById('desde').value; 
    let hasta = document.getElementById('hasta').value;
    let mensaje = document.getElementById('mensaje').value;
    let salida = document.getElementById('salida').value; 
    let llegada = document.getElementById('llegada').value;
    let lunes = document.getElementById('lun').checked;
    let martes = document.getElementById('mar').checked;
    let miercoles = document.getElementById('mie').checked;
    let jueves = document.getElementById('jue').checked;
    let viernes = document.getElementById('vie').checked;
    let sabado = document.getElementById('sab').checked;
    let domingo = document.getElementById('dom').checked;
   
const dato = {
   nombre,
   desde,
   hasta,
   mensaje,
   salida,
   llegada,
   lunes,
   martes,
   miercoles,
   jueves,
   viernes,
    sabado,
    domingo
};

return dato;
}



 /**
  * Obtiene a un usuaio de una tabla
  */
 function obtenerUsuario(tableName) {
   let nombreUsuario="";
   let session= obtenerDatosSession(tableName);
   for(let i in session){
     nombreUsuario= session[i].nombreUsuario;
     break;
   }
   return nombreUsuario;
 }

 /**
  * Obtiene el id de una tabla de usuarios
  * @param nombreTabla nombre de la tabla
  */
 function obtenerIdUsuario(nombreTabla,nombreUsuario){
   let nuevo=obtenerDatosTabla(nombreTabla);
   let id="";
   for(let i in nuevo){
     if(nombreUsuario==nuevo[i].nombreUsuario){
       id=nuevo[i].id;
       break;
     }
   }
   return id;
 }

/**
 * metodos de localStorage y sessionStorage
 */
 /**
 


 /**
  * Inserta los datos del usuaroio en la tabla usuario del localStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
*/
  function insertarEnTabla(tableName, object) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
      tableData = [];
    }
    let primaryKey = tableData.length + 1;
    object.id = primaryKey;
    tableData.push(object);
    localStorage.setItem(tableName, JSON.stringify(tableData));
    return tableData;
  }  

  /**
   * Obtener datos de usuario
   *
   * @param {*} tableName nombre de la tabla
   */
  function obtenerDatosTabla(tableName) {
    let tableData = JSON.parse(localStorage.getItem('nuevo'));
    console.log(tableData);
    if (!tableData) {
      tableData = [];
    }
    return tableData;
  }

  /**
   *
   * @param {*} llave llave para guardar
   * @param {*} valor valor asociado a la llave
   */
  function guardarLocalStorage(llave, valor){
    localStorage.setItem(key, JSON.stringify(valor));
    return true;
  }

 /**
  * Inserta cualquier objeto en la tabla del sessionStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
  */
function guardarSession(tableName, object){
  let tableData = JSON.parse(sessionStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  let primaryKey = tableData.length + 1;
  object.id = primaryKey;
  tableData.push(object);
  sessionStorage.setItem(tableName, JSON.stringify(tableData));
  return tableData;
}

/**
 * obtiene los datos de una tabla en el sessionStorage
 * @param tableName nombre de la tabla
 */
function obtenerDatosSession(tableName) {
  let tableData = JSON.parse(sessionStorage.getItem(tableName));
  
  if (!tableData) {
    tableData = [];
  }
  return tableData;
}


 function verificarRegistro(){
   let nombreUsuario = document.getElementById('name').value;
   let nuevo=obtenerDatosTabla('nuevo');
   let user="";
   for(let i in nuevo){
     if(nombreUsuario==nuevo[i].nombreUsuario){
       user=nuevo[i].nombreUsuario;
       break;
     }
   }
   let dato= guardarDato();
   if(!dato.nombre || !dato.desde || !dato.hasta || !dato.mensaje || !dato.salida || !dato.llegada  ){
     window.alert("Por favor llenar todos los campos");
   }else{
       if(user!=dato){
         dato=insertarEnTabla('nuevo', dato);
       }else {
         window.alert("el dato ya existe");
       }
    
   }
}


function verificarCamposInicio(campoUno, campoDos, nombreTabla, datosTabla){
  if(!campoUno || !campoDos){
    window.alert("No olvides llenar los campos");
  }else{
    renderizarTablaBusqueda(campoUno, campoDos, nombreTabla, datosTabla);
  }
}



 function eventos() {
 	jQuery('#boton').bind('click', (element) => {
 		verificarRegistro();
 	});
}

 eventos();
