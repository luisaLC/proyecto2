function guardarUsuario(){
    let nombre = document.getElementById('name').value;
    let apellido = document.getElementById('lname').value; 
    let telefono = document.getElementById('cell').value;
    let nombreUsuario = document.getElementById('nUsuario').value; 
    let repContrasena = document.getElementById('passEqual').value;

const usuario = {
   nombre,
   apellido,
   telefono,
   nombreUsuario,
   repContrasena
};

return usuario;
}

 /**
  * guarda los datos necesarios en el sessionStorage
  */
 function datosUsuarioSession() {
   let nombreUsuario = document.getElementById('UserName').value;
   let idUsuario= obtenerIdUsuario('usuarios', nombreUsuario);

   const usuarioSession={
     nombreUsuario,
     idUsuario
   };
   const usuariosSession = guardarSession('UserName',usuarioSession);
 }

 /**
  * Obtiene a un usuaio de una tabla
  */
 function obtenerUsuario(tableName) {
   let nombreUsuario="";
   let session= obtenerDatosSession(tableName);
   for(let i in session){
     nombreUsuario= session[i].nombreUsuario;
     break;
   }
   return nombreUsuario;
 }

 /**
  * Obtiene el id de una tabla de usuarios
  * @param nombreTabla nombre de la tabla
  */
 function obtenerIdUsuario(nombreTabla,nombreUsuario){
   let usuarios=obtenerDatosTabla(nombreTabla);
   let id="";
   for(let i in usuarios){
     if(nombreUsuario==usuarios[i].nombreUsuario){
       id=usuarios[i].id;
       break;
     }
   }
   return id;
 }

/**
 * metodos de localStorage y sessionStorage
 */
 /**
 


 /**
  * Inserta los datos del usuaroio en la tabla usuario del localStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
*/
  function insertarEnTabla(tableName, object) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
      tableData = [];
    }
    let primaryKey = tableData.length + 1;
    object.id = primaryKey;
    tableData.push(object);
    localStorage.setItem(tableName, JSON.stringify(tableData));
    return tableData;
  }  

  /**
   * Obtener datos de usuario
   *
   * @param {*} tableName nombre de la tabla
   */
  function obtenerDatosTabla(tableName) {
    let tableData = JSON.parse(localStorage.getItem('usuarios'));
    console.log(tableData);
    if (!tableData) {
      tableData = [];
    }
    return tableData;
  }

  /**
   *
   * @param {*} llave llave para guardar
   * @param {*} valor valor asociado a la llave
   */
  function guardarLocalStorage(llave, valor){
    localStorage.setItem(key, JSON.stringify(valor));
    return true;
  }

 /**
  * Inserta cualquier objeto en la tabla del sessionStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
  */
function guardarSession(tableName, object){
  let tableData = JSON.parse(sessionStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  let primaryKey = tableData.length + 1;
  object.id = primaryKey;
  tableData.push(object);
  sessionStorage.setItem(tableName, JSON.stringify(tableData));
  return tableData;
}

/**
 * obtiene los datos de una tabla en el sessionStorage
 * @param tableName nombre de la tabla
 */
function obtenerDatosSession(tableName) {
  let tableData = JSON.parse(sessionStorage.getItem(tableName));
  
  if (!tableData) {
    tableData = [];
  }
  return tableData;
}

/**
 * Metodos login
 */

 function validarUsuario(){
   let usuario = document.getElementById('UserName').value;
   let pass = document.getElementById('UserPass').value;
   let usuariosS = obtenerDatosTabla('usuarios');

   for(let i in usuariosS){
     if (usuario==usuariosS[i].nombreUsuario && pass==usuariosS[i].repContrasena) {
          window.alert("............");
         window.location.href='../perfil.html', true
         break;
     }else{
          window.alert("Usuario incorrecto");
     }
   }
 }


 function verificarRegistro(){
   let nombreUsuario = document.getElementById('nUsuario').value;
   let usuarios=obtenerDatosTabla('usuarios');
   let user="";
   for(let i in usuarios){
     if(nombreUsuario==usuarios[i].nombreUsuario){
       user=usuarios[i].nombreUsuario;
       break;
     }
   }
   let usuario= guardarUsuario();
   if(!usuario.nombre || !usuario.apellido || !usuario.telefono ||
   !usuario.nombreUsuario || !usuario.repContrasena || !document.getElementById('passEqual').value){
     window.alert("Por favor llenar todos los campos");
   }else{
     if(document.getElementById('pass').value==document.getElementById('passEqual').value){
       if(user!=nombreUsuario){
         usuario=insertarEnTabla('usuarios', usuario);
       }else {
         window.alert("el usuario ya existe");
       }
      }else {
        window.alert("las contraseñas no coinciden");
      }
   }
}


function verificarCamposInicio(campoUno, campoDos, nombreTabla, datosTabla){
  if(!campoUno || !campoDos){
    window.alert("No olvides llenar los campos");
  }else{
    renderizarTablaBusqueda(campoUno, campoDos, nombreTabla, datosTabla);
  }
}





 function eventos() {
 	jQuery('#btnReg').bind('click', (element) => {
 		verificarRegistro();
 	});
  jQuery('#btnIS').bind('click',(element) =>{
    validarUsuario();
   // datosUsuarioSession();
  });
 }

 eventos();
